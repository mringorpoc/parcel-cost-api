package com.example.apigateway.parcel.calculator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class Controller {

	ParcelCostCalculatorClient parcelCostCalculatorClient;

	@Autowired
	public Controller(ParcelCostCalculatorClient parcelCostCalculatorClient) {
		this.parcelCostCalculatorClient = parcelCostCalculatorClient;
	}

	@PostMapping("/parcel/price")
	@CrossOrigin
	ParcelResponse getPrice(@RequestBody ParcelRequest parcelInput) {
		return parcelCostCalculatorClient.getPrice(parcelInput);
	}
}
