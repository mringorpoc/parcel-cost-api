package com.example.apigateway.parcel.calculator;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("PARCEL-COST-CALCULATOR")
public interface ParcelCostCalculatorClient {

	@PostMapping(value="/api/parcel/price")
    @CrossOrigin
    ParcelResponse getPrice(@RequestBody ParcelRequest parcelInput);
}
