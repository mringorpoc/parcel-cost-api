# README #

# parcel cost calculator microservice
Parcel cost calculator spring boot, eureka, feign and drools for the rules engine.

## Description

This project is just a demo. Different components can be used in production. e.g. kubernetes cloud over eureka. ibm api gateway instead of spring-boot api gateway.
Later, a better api-gateway that will protect internal microservices with an api key with internal circuit breaker should be added. 
Internal microservices are made of springboot which talks to each other using feign (can be improved to use kafka) and registered to eureka (service discovery).

The parcel-cost-calculator loads voucher from a remote api https://mynt/voucher through a proxy.

The rules is in price.drl file located in src/main/resources. This can be modified and connected to a database or a rules UI like camunda.

![Scheme](mynt.png)

### Usage

Go to https://parcel-cost.mringork8s.one/swagger-ui.html and click try it out as described below.

![Scheme](swagger-1.png)

![Scheme](swagger-2.png)

### Build discovery-service
```
cd discovery-service
./mvnw clean package spring-boot:repackage
```

### Build api-gateway
Layer on top of internal microservices.
```
cd api-gateway
./mvnw clean package spring-boot:repackage
```

### Build product-service
```
cd parcel-cost-calculator
./mvnw clean package spring-boot:repackage
```

### Dockerize them all
```
cd root
docker-compose up -d
```

### Check discovered services
```
http://localhost:8761/
```

### Health check api
```
https://parcel-cost.mringork8s.one/actuators/health
```

### Check open api specification
```
https://parcel-cost.mringork8s.one/swagger-ui.html
```