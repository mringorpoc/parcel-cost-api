package com.reactive.parcelcostcalculator.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public final class DateUtils {

	private static final DateTimeFormatter YYYY_MM_DD = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	
	private DateUtils () {}
	
	public static final LocalDate fromStringToLocalDate (String date) {
		return LocalDate.parse(date, YYYY_MM_DD);
	}
}
