package com.reactive.parcelcostcalculator.api.model;

public class Parcel {

	private final Double volume;
	private final Double weight;
	private Double price = 0d;
	private int priority = 1;

	private Parcel(Double volume, Double weight) {
		this.volume = volume;
		this.weight = weight;
	}
	
	public static class Builder {
		
		private Double weight;
		private Double width;
		private Double height;
		private Double length;
		
		public Builder weight(Double weight) {
			this.weight = weight;
			return this;
		}
		
		public Builder height(Double height) {
			this.height = height;
			return this;
		}
		
		public Builder length(Double length) {
			this.length = length;
			return this;
		}
		
		public Builder width(Double width) {
			this.width = width;
			return this;
		}
		
		public Parcel build() {
			Double volume = this.width * this.height * this.length;
			return new Parcel(volume, this.weight);
		}
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getVolume() {
		return volume;
	}

	public Double getWeight() {
		return weight;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}
}
