package com.reactive.parcelcostcalculator.api.service;

import org.springframework.web.bind.annotation.CrossOrigin;

import com.reactive.parcelcostcalculator.api.model.Discount;

import feign.Param;
import feign.RequestLine;

public interface DiscountService {

	@RequestLine("GET /voucher/{code}?key=apikey")
	@CrossOrigin
    Discount getDiscount(@Param("code") String code);
}
