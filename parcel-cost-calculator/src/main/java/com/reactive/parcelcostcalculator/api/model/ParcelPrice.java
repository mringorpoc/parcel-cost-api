package com.reactive.parcelcostcalculator.api.model;

import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

import com.reactive.parcelcostcalculator.utils.DateUtils;

public class ParcelPrice {
	
	private final Integer priority;
	
	private final Double price;
	
	private final Double discountedPrice;
	
	private final Discount discount;
	
	private final Type type;
	
	public ParcelPrice(Parcel parcel, Discount discount){
		
		this.priority = parcel.getPriority();
		this.price = parcel.getPrice();
		this.discount = discount;
		
		LocalDate expiryDate = DateUtils.fromStringToLocalDate(Optional.ofNullable(discount.getExpiry()).orElse("1900-01-01"));
		
		if (LocalDate.now().isBefore(expiryDate) || LocalDate.now().isEqual(expiryDate)) {
			this.discountedPrice = price - Optional.ofNullable(discount.getDiscount()).orElse(0d);
		} 
		
		else {
			this.discount.addMessage("Discount code is expired.");
			this.discountedPrice = this.price;
		}
		
		this.type = Type.getByPriority(this.priority);
	}

	public Integer getPriority() {
		return priority;
	}

	public Double getPrice() {
		return price;
	}

	public Double getDiscountedPrice() {
		return discountedPrice;
	}

	public Discount getDiscount() {
		return discount;
	}
	
	public Type getType() {
		return type;
	}

	public enum Type {

		Reject(1), HeavyParcel(2), LargeParcel(5), MediumParcel(4), SmallParcel(3);

		private final int priority;

		Type (int priority) {
			this.priority = priority;
			ReverseStorage.reverseMap.put(priority, this);
		}

		public static Type getByPriority(int priority) {
			return Optional.ofNullable(ReverseStorage.reverseMap.get(priority)).orElse(Reject);
		}

		private static final class ReverseStorage {
			private static final Map<Integer, Type> reverseMap = new LinkedHashMap<>();
		}

		public int getPriority() {
			return priority;
		}
	}
}
