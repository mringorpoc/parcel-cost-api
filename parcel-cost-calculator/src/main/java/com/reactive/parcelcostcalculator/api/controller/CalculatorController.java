package com.reactive.parcelcostcalculator.api.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.reactive.parcelcostcalculator.api.model.Discount;
import com.reactive.parcelcostcalculator.api.model.Parcel;
import com.reactive.parcelcostcalculator.api.model.ParcelInput;
import com.reactive.parcelcostcalculator.api.model.ParcelPrice;
import com.reactive.parcelcostcalculator.api.service.CostCalculator;
import com.reactive.parcelcostcalculator.api.service.DiscountServiceFactory;

@RestController
@RequestMapping("/api")
public class CalculatorController {

	CostCalculator costCalculator;
	DiscountServiceFactory discountServiceFactory;
	
	private static final Logger logger = LoggerFactory.getLogger(CalculatorController.class);
	
    @Autowired
    public CalculatorController(CostCalculator costCalculator, DiscountServiceFactory discountServiceFactory) {
    	
        this.costCalculator = costCalculator;
        this.discountServiceFactory = discountServiceFactory;
    }

	@PostMapping(value = "/parcel/price")
	public ParcelPrice price(@Valid @RequestBody ParcelInput parcelInput) {

		Discount discount = voucher(parcelInput.getVoucherCode());

		return costCalculator.calculate(new Parcel.Builder()
		    		   .weight(parcelInput.getWeight())
		    		   .width(parcelInput.getWidth())
					   .height(parcelInput.getHeight())
					   .length(parcelInput.getLength())
					   .build(), discount);
	}

	private Discount voucher(String code) {

		final Discount NO_DISCOUNT = new Discount(code, 0d, "2021-08-26");

		try {

			// there should be a better implementation in the discount api.
			// code should not be an enum
			String voucher = Discount.VoucherCode.valueOf(Optional.ofNullable(code)
					.orElse("NONE")).name();

			if ("NONE".equalsIgnoreCase(voucher)) {
				return  NO_DISCOUNT.addMessage("Voucher code " +code+ " does not exist.");
			}
			
			return discountServiceFactory.myntDiscountService().getDiscount(voucher);

		} catch (Exception e) {
			logger.error("Error with input voucher(" + code + ")", e);
			return NO_DISCOUNT.addMessage(e.getLocalizedMessage());
		}
	}
}
