package com.reactive.parcelcostcalculator.api.service;

import com.reactive.parcelcostcalculator.api.model.Discount;
import com.reactive.parcelcostcalculator.api.model.Parcel;
import com.reactive.parcelcostcalculator.api.model.ParcelPrice;

public interface CostCalculator {
	ParcelPrice calculate(Parcel parcel, Discount discount);
}
