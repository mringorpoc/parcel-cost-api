package com.reactive.parcelcostcalculator.api.model;

public class Discount {

	private String code;
	private Double discount;
	private String expiry;
	private String message = "";
	
	public Discount() {
		super();
	}
	
	public Discount(String code, Double discount, String expiry) {
		this.code = code;
		this.discount = discount;
		this.expiry = expiry;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	public String getExpiry() {
		return expiry;
	}
	public void setExpiry(String expiry) {
		this.expiry = expiry;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public Discount addMessage(String message) {
		setMessage(message);
		return this;
	}

	//shouldn't be fixed
	public enum VoucherCode {
		MYNT, GFI, skdlks, NONE
	}

	@Override
	public String toString() {
		return "Discount [code=" + code + ", discount=" + discount + ", expiry=" + expiry + "]";
	}
}
