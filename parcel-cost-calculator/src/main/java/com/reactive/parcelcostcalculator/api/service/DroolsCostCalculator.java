package com.reactive.parcelcostcalculator.api.service;

import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.reactive.parcelcostcalculator.api.model.Discount;
import com.reactive.parcelcostcalculator.api.model.Parcel;
import com.reactive.parcelcostcalculator.api.model.ParcelPrice;

@Service
public class DroolsCostCalculator implements CostCalculator {

	@Autowired
	KieSession rules;

	
	@Override
	public ParcelPrice calculate(Parcel parcel, Discount discount) {
		rules.insert(parcel);
		rules.fireAllRules();
		return new ParcelPrice(parcel, discount);
	}
}
