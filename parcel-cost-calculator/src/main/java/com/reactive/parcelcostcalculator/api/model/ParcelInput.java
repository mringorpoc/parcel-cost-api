package com.reactive.parcelcostcalculator.api.model;

import java.util.Optional;

public class ParcelInput {
	
	private Double weight = 0d;
	private Double height = 0d;
	private Double width = 0d;
	private Double length = 0d;
	private String voucherCode;
	
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = Optional.ofNullable(weight).orElse(0d);
	}
	
	public Double getHeight() {
		return height;
	}
	public void setHeight(Double height) {
		this.height = Optional.ofNullable(height).orElse(0d);
	}
	
	public Double getWidth() {
		return width;
	}
	public void setWidth(Double width) {
		this.width = Optional.ofNullable(width).orElse(0d);
	}
	
	public Double getLength() {
		return length;
	}
	public void setLength(Double length) {
		this.length = Optional.ofNullable(length).orElse(0d);
	}
	
	public String getVoucherCode() {
		return voucherCode;
	}
	public void setVoucherCode(String voucherCode) {
		this.voucherCode = voucherCode;
	}
}
