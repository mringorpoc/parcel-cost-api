package com.reactive.parcelcostcalculator.api.service;

import org.springframework.stereotype.Component;

import feign.Feign;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;
import feign.okhttp.OkHttpClient;

@Component
public class DiscountServiceFactory {

	DiscountService discountService;

	private String myntServiceURI = "https://mynt-exam.mocklab.io";

	DiscountServiceFactory (){
		
		this.discountService = Feign.builder().client(new OkHttpClient())
				.encoder(new GsonEncoder())
				.decoder(new GsonDecoder()).logger(new feign.slf4j.Slf4jLogger(DiscountService.class))
				.logLevel(feign.Logger.Level.FULL)
				.target(DiscountService.class, myntServiceURI);
	}
	
	public final DiscountService myntDiscountService() {
		return discountService;
	}
}
